#  Copyright (c) 2019. @ Mirafra Technologies Pvt. Ltd. By Brijesh kumar Email: Brijeshkumar@mirafra.com

import requests
import json as JSON
BASE_USER_GROUP_INFO="http://192.168.1.24:8000/api/groups"

def get_groups():
    url=BASE_USER_GROUP_INFO
    try:
        res=requests.get(url)
        if res.status_code==200:

            return res.content
    except ConnectionError:
        pass

def create_group(data):
    url = BASE_USER_GROUP_INFO
    try:
        res = requests.post(url+'/',data=data)
        if res.status_code == 200:
            return res.text
    except ConnectionError:
        pass

def get_scope(group):
    return ("read","delete")



"""ci_cd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include,re_path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title='CI/CD APIs',urlconf='users.urls')



from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger import renderers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

class JSONOpenAPIRenderer(renderers.OpenAPIRenderer):
    media_type = 'application/json'

class SwaggerSchemaView(APIView):
    permission_classes = [AllowAny]
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer,
        renderers.JSONRenderer,
        JSONOpenAPIRenderer

    ]

    def get(self, request):
        generator = SchemaGenerator()
        schema = generator.get_schema(request=request)


        return Response(schema)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include("users.urls")),
    path('docs/', SwaggerSchemaView.as_view()),
    path('docs2/', schema_view),
    path('notifications/', include('django_nyt.urls')),
    path('wiki/', include('wiki.urls')),
    re_path(r'^users/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^users/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#  Copyright (c) 2019. @ Mirafra Technologies Pvt. Ltd. By Brijesh kumar Email: Brijeshkumar@mirafra.com

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .forms import EmailUserChangeForm, EmailUserCreationForm
from .models import EmailUser, UserMediaFiles, IdeaModel, CommentIdeaModel, VoteIdeaModel


class EmailUserAdmin(UserAdmin):

    """EmailUser Admin model."""

    fieldsets = (
        (None, {'fields': ('email','first_name','last_name','emp_id','emp_desig','phone','user_group', 'password')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = ((
        None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')
        }
    ),
    )

    # The forms to add and change user instances
    form = EmailUserChangeForm
    add_form = EmailUserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email','first_name','last_name','emp_id','emp_desig','phone','user_group', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email','user_group')
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)


# Register the new EmailUserAdmin
admin.site.register(EmailUser, EmailUserAdmin)
@admin.register(UserMediaFiles)
class UserMediaFilesAdmin(admin.ModelAdmin):
    list_display = ["user","media","type_of_media",'created']


@admin.register(IdeaModel)
class UserIdeaAdmin(admin.ModelAdmin):
    list_display = ["user","idea","title",'created']

@admin.register(CommentIdeaModel)
class IdeaCommentAdmin(admin.ModelAdmin):
    list_display = ["user","idea_id","comment","parent",'created','modified']

@admin.register(VoteIdeaModel)
class IdeaVoteAdmin(admin.ModelAdmin):
    list_display = ["user","idea_id","like",'created','modified']
#  Copyright (c) 2019. @ Mirafra Technologies Pvt. Ltd. By Brijesh kumar Email: Brijeshkumar@mirafra.com

from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'

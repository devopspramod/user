#  Copyright (c) 2019. @ Mirafra Technologies Pvt. Ltd. By Brijesh kumar Email: Brijeshkumar@mirafra.com

from rest_framework import serializers
from .models import EmailUser, IdeaModel


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model=EmailUser
        fields=["id","email","first_name","last_name","emp_id","user_group","emp_desig","phone"]


class UserCreateSerializer(serializers.Serializer):
    email=serializers.CharField(max_length=50)
    first_name=serializers.CharField(max_length=50)
    last_name = serializers.CharField(max_length=50)

class UserUpdateSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=50)
    emp_id=serializers.CharField(max_length=50)
    emp_desig=serializers.CharField(max_length=50)
    phone = serializers.CharField(max_length=50)

class UserLoginSerializer(serializers.Serializer):
    email=serializers.CharField(max_length=50)
    password=serializers.CharField(max_length=50)

class UserSetPasswordSerializer(serializers.Serializer):
    password1=serializers.CharField(max_length=50)
    password2=serializers.CharField(max_length=50)

class AssignUserGroupSerializer(serializers.Serializer):
    email=serializers.CharField(max_length=50)
    user_group=serializers.CharField(max_length=50)

class DeleteUserSerializer(serializers.Serializer):
    email=serializers.CharField(max_length=50)


class ManageUserGroupSerializer(serializers.Serializer):
    user_group_name=serializers.CharField(max_length=50)

class IdeationSerializer(serializers.Serializer):
    id=serializers.ReadOnlyField()
    user=serializers.CharField(max_length=100)
    idea = serializers.CharField(max_length=500)
    title = serializers.CharField(max_length=500)
    desc = serializers.CharField(max_length=10000)
    created=serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    # modified=serializers.CharField(max_length=100)

class CommentBaseSerializer(serializers.Serializer):
    user=serializers.CharField(max_length=100)
    idea_id=serializers.CharField(source='idea_id.id',read_only=True)
    comment=serializers.CharField(max_length=10000)
    parent_comment=serializers.CharField(source='parent_comment.id',read_only=True)

class CommentGetSerializer(serializers.Serializer):
    id=serializers.CharField(read_only=True)
    user=serializers.CharField(max_length=100)
    idea_id=serializers.CharField(source='idea_id.id',read_only=True)
    comment=serializers.CharField(max_length=10000)
    parent = serializers.CharField(source='parent.id',read_only=True)
    created=serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    modified=serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

class VoteIdeaSerializer(serializers.Serializer):
    user=serializers.CharField(max_length=100)
    idea_id=serializers.CharField(source='idea_id.id',read_only=True)
    like=serializers.BooleanField()

class TokenSerializer(serializers.Serializer):
    access=serializers.CharField(max_length=100)
    refresh=serializers.CharField(max_length=100)



#  Copyright (c) 2019. @ Mirafra Technologies Pvt. Ltd. By Brijesh kumar Email: Brijeshkumar@mirafra.com

from django.contrib import admin
from django.urls import path, re_path
from users import views

# urlpatterns = [
#     path('getUser/', views.UserView.as_view({'get': 'retrieve'})),
#     path('getAllUser/', views.UserView.as_view({'get': 'list'})),
# ]

from rest_framework.routers import DefaultRouter
from django.views.decorators.csrf import csrf_exempt
router = DefaultRouter()
router.register(r'users', views.UserView, basename='user')
#router.register(r'manages', views.UserCreateUpdateDeleteView,basename="manages")
#router.register(r'login', views.LoginLogoutView, basename='user')

urlpatterns = router.urls

urlpatterns+= [
    #path('loginn/', views._login,name="login"),
    path('login/', views.LoginView.as_view(),name="loginn"),
    # path('logout/', views._logout,name="logout"),
    path('logout', views.LogoutView.as_view(), name="logout"),
    path('add/', views.UserCreateView.as_view(),name="create_user"),
    path('manages/', views.UserUpdateView.as_view(),name="update_user"),
    re_path('^delete/(?P<email>)/$', views.DeletUserView.as_view(),name="delete_user"),
    #path('get_user_profile/',views.GetUser.as_view()),
    #path('set_passwordd/', views.setPassword,name="set_password"),
    path('set_password/', views.SetPasswordView.as_view(),name="set_passwordd"),
    path('reset_password/', views.setPasswordRequest,name="reset_password_request"),
    # path('admin/create_group/', views.CreateGroupView.as_view(),name="create_group"),
    # path('admin/assign_group/', views.AssignUserGroup.as_view(),name="assign_group"),
    # path('admin/search/', views.SerchUserView.as_view(),name="search"),
    # path('admin/users_in_group/', views.Users_InGroup, name="users_in_group"),
    # path('check_user_authenticated/',views.check_user),
    path('ideation/', views.IdeationViews.as_view()),
    path('ideation/comments', views.CommentsView.as_view()),
    path('ideation/vote', views.VoteView.as_view()),
    path('get_group_users',views.GetUserView.as_view()),
    path('get_all_user',views.GetAllUser.as_view()),
]
#  Copyright (c) 2019. @ Mirafra Technologies Pvt. Ltd. By Brijesh kumar Email: Brijeshkumar@mirafra.com

from rest_framework.permissions import BasePermission
from .models import EmailUser
import requests

class IsUserGroupAdmin(BasePermission):
    """
    Allows access only to "is_active" users.
    """
    def has_permission(self, request, view):
        userfromdb=EmailUser.objects.get(email=request.user.email)
        if userfromdb.user_group=="admin":
            print( userfromdb.user_group)
            return request.user and  True
        else:
            return request.user and False


class IsUserGroupAdmin2(BasePermission):
    """
    Allows access only to "is_active" users.
    """
    def has_permission(self, request, view):
        url=""
        data=requests.get(url+"/"+request.user)
        # userfromdb=EmailUser.objects.get(email=request.user.email)
        if data=="admin":
            # print( userfromdb.user_group)
            return request.user and  True
        else:
            return request.user and False

